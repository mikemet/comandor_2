$(function() {
	filterOrders();
	$('#ordersFilter :input').change(function() {
		filterOrders();
	});
	
	$("#resetFilters").click(function(){
		$('#ordersFilter :input').off('change');
		$('#ordersFilter select[name="lifeCycle"]').val('в работе');
		$('#ordersFilter .inputDate').val('');
		$('#ordersFilter #ipSearch').val('');
		$('#ordersFilter #sortOrdersBy').val('deliveryTimestamp');
		filterOrders();
		$('#ordersFilter :input').change(function() {
			filterOrders();
		});
	});		

});

function filterOrders() {
	var startDate 	= $('.inputDate[name="startDate"]').datepicker('getDate'),
		endDate 	= $('.inputDate[name="endDate"]').datepicker('getDate'),
		lifeCycle 	= $('#ordersFilter select[name="lifeCycle"]').val();

	if (startDate != null)
		startDate = startDate.getTime() / 1000;
	if (endDate != null)
		endDate = endDate.getTime() / 1000 + 86399;
	else
		endDate = 99999999999999999;

	if ($('#ipSearch').val() == '')
		var dealer = 'anyone';
	else
		var dealer = $('#ipSearch').val();

	var $filteredData = listClone.find('li');
	$filteredData = $filteredData.filter(function() {
		var action = 'show';
		if ($(this).data('lifecycle') != lifeCycle)
			action = 'hide';
		if ($(this).data('ordertimestamp') > endDate || $(this).data('ordertimestamp') < startDate)
			action = 'hide';
		if (dealer != 'anyone' && $(this).find('a span.orderDealer').text() != dealer)
			action = 'hide';

		if (action == 'show')
			return true;
		else
			return false;
	});

	$orderBy = $('#sortOrdersBy :selected').text();

	if ($orderBy == "По номеру заказа") {
		// if sorted by id
		var $sortedData = $filteredData.sorted({
			by : function(v) {
				return $(v).data("id");
			}
		});
	} else if ($orderBy == "По ИП") {
		// if sorted by customer
		var $sortedData = $filteredData.sorted({
			by : function(v) {
				return $(v).find('a span.orderDealer').text().toLowerCase();
			}
		});
	} else if ($orderBy == "По дате отгрузки") {
		//if sorted by delivery date
		var $sortedData = $filteredData.sorted({
			by : function(v) {
				return $(v).data('deliverytimestamp');
			}
		});
	}
	/* else if ($orderBy == "По оплате"){
	 //if sorted by payment
	 sortUsingNestedText($('.ordersList'), 'li', 'span.orderCustomer');
	 var $sortedData = $filteredData.sorted({
	 reversed: true,
	 by: function(v) {
	 return $(v).find('a span.orderStatus').text().toLowerCase();
	 }
	 });
	 } */
	else {
		//if sorted by letter
		var $sortedData = $filteredData.sorted({
			by : function(v) {
				return $(v).find('span.orderPrefix').text();
			}
		});
	}
	;
	list.quicksand($sortedData, {
		adjustWidth : false,
		duration : 800,
		easing : 'easeInOutQuad'
	});
}	